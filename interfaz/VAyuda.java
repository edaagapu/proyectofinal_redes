package interfaz;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import java.awt.Color;

public class VAyuda extends JFrame {

	private JPanel contentPane;

	public VAyuda() {
		setTitle("Acerca de..");
		setBounds(500, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 12, 426, 211);
		contentPane.add(scrollPane);

		JTextArea txtrAcercaDe = new JTextArea();
		txtrAcercaDe.setLineWrap(true);
		txtrAcercaDe.setWrapStyleWord(true);
		txtrAcercaDe.setText(
				"El trabajo fue realizado por:\n\n- Edwin Aarón Garcia Pulido\n- Jhon Alejandro Morales Mojica \n- Sergio Alejandro Gomez Lara\n\nPara mas información sobre el aplicativo se puede encontrar en el repositorio en GitLab (https://gitlab.com/edaagapu/proyectofinal_redes)");
		txtrAcercaDe.setEditable(false);
		scrollPane.setViewportView(txtrAcercaDe);
	}
}
