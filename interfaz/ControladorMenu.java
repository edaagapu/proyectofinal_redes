package interfaz;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JOptionPane;

import hamming.interfaz.VentanaPriHamm;
import huffman.interfaz.VEntradaHuffman;
import manchester.interfaz.VEntradaManchester;
import manchester.interfaz.VManchester;
import manchester.logica.Codigo;

public class ControladorMenu implements ActionListener {
	private VMenu ventana;

	public ControladorMenu(VMenu ventana) {
		this.ventana = ventana;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JButton boton = (JButton) e.getSource();
		for (int i = 0; i < ventana.botones.length; i++) {
			if (ventana.botones[i].equals(boton)) {
				accion(i);
			}
		}
	}

	private void accion(int i) {
		switch (i) {
		case 0:
			VEntradaManchester vManchester = new VEntradaManchester();
			vManchester.setVisible(true);
			ventana.dispose();
			break;
		case 1:
			VEntradaHuffman vHuffman = new VEntradaHuffman();
			vHuffman.setVisible(true);
			ventana.dispose();
			break;
		case 2:
			VentanaPriHamm frame = new VentanaPriHamm();
			frame.setVisible(true);
			ventana.dispose();
			break;
		case 3:
			VAyuda vAyuda = new VAyuda();
			vAyuda.setVisible(true);
			break;
		case 4:
			System.exit(0);
			break;
		default:
			break;
		}
	}

}
