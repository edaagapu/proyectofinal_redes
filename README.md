# ProyectoFinal_Redes
<br>
Proyecto de Redes de Computación II (Grupo 82)

Integrantes del Equipo:
- Edwin Aarón Garcia Pulido (@edaagapu)
- Sergio Alejandro Gómez Lara (@saglara)
- Jhon Alejandro Morales Mojica (@alejomora999)

<hr>

# Contenido

1. Introducción
2. Propósito
3. Medios usados

<hr>

# Introducción

El presente proyecto, fue generado como una alternativa didáctica para reforzar el conocimiento de la codificación y decodificación, mediante distintos métodos, enfocandonos principalmente en el [Algoritmo de Huffman](https://www.youtube.com/watch?v=zT2p-xD0NFw&t=96s) , el [Código de Hamming](https://www.youtube.com/watch?v=zg06eShv6ok)  y en la [Codificación Manchester](https://www.youtube.com/watch?v=NkXUWEEZpqg), se procura desarrollar de forma modular, para facilidad del equipo de trabajo.

<hr>

# Propósito

El objetivo del proyecto es poder desarrollar de forma didáctica varios métodos de codificación, para reforzar el conocimiento del tema y a su vez, poder apoyar el conocimiento del área en cuestión.

<hr>

# Medios Usados

El proyecto se realizo mediante el lenguaje de Java, para ser ejecutado en escritorio.

<hr>

# Ejecución del jar

Sin importar la plataforma, ejecutar en linea de comandos java -jar Proyecto2.jar

<hr>

# Licencias

GPL V3
