package hamming.logica;

import javax.swing.JOptionPane;

public class Algoritmo {
	
	private String datoOriginal;
	private int[] bits_par;
	private int[] bits_datos;
	private String[][] resultadoCod;
	private String[][] resultadoDec;
	private String[][] binarios;
	
	
	public Algoritmo(String datoOriginal) {
		this.datoOriginal = datoOriginal;
	}
	
	public void imprimir_Resultado(int filas,int cols,String [][]res) { 
		for (int i = 0; i < filas; i++) {	
			for (int j = 0; j < cols; j++) {
				System.out.print(res[i][j]+" ");
			}
			System.out.println("");
		}
	}
	
	public void numerosBinarios(int filBin, int colBin) {
		
		binarios=new String[filBin][colBin];
		
		int contAB=0;
		
		for (int i = 0; i < filBin; i++) {
			String bin = Long.toBinaryString(i+1);
			for (int j = 0; j < colBin; j++) {				
				
				if(colBin<=bin.length()+j) {
					String[] numsBin=bin.split("");					
					binarios[i][j]=numsBin[contAB];

					contAB+=1;
				}else {
					binarios[i][j]="0";
				}
			}
			contAB=0;
		}
	}

	public void codificacion() {
		bits_par= new int[cant_paridad()];//Numero Bits Paridad
		bits_datos= new int[getNumHamm_2()];//Cantidad de Datos
		
		resultadoCod= new String[bits_par.length+2][bits_par.length + bits_datos.length];	
						
		bits_parCod();
		bits_datoCod();
		
		int cont_par=0;
		int cont_datos=0;
		int aux_par=cant_paridad()-1;
		
		numerosBinarios(getNumHamm_1Cod(),cant_paridad());		
				
		for (int i = 0; i < getFilasResultadoCod(); i++) {	
			
			for (int j = 0; j < getNumHamm_1Cod(); j++) {
				resultadoCod[i][j]="0";				
				if(i==0) {					
					if(cont_par<cant_paridad() && j==bits_par[cont_par] ) {						
						resultadoCod[i][j]="-";
						cont_par+=1;
					}else {		
						resultadoCod[i][j]=""+bits_datos[cont_datos];
						cont_datos+=1;
					}
				}
				else{														
					if(cont_par<cant_paridad() && j==bits_par[cont_par] ) {
						resultadoCod[i][j]="-";											
						cont_par+=1;
					}else if (i<getFilasResultadoCod()-1) {
						String[] auxBin = binarios[j];					 
						if(auxBin[aux_par].equals("1"))
						{	
							resultadoCod[i][j]=""+bits_datos[cont_datos];		
						}else {
							resultadoCod[i][j]="-";							
						}	
						cont_datos+=1;						
					}					
				}						
			}

			if(i>0 && i<getFilasResultadoCod()-1 && XOR(resultadoCod[i],resultadoCod[i].length)){
				//PAR
				resultadoCod[i][bits_par[i-1]]="0";
			}else if(i>0 && i<getFilasResultadoCod()-1){
				//IMPAR
				resultadoCod[i][bits_par[i-1]]="1";
			}			
			cont_par=0;
			cont_datos=0;
			
			if(i>0) {
				aux_par-=1;
			}

		}
		
		int comodin=getNumHamm_1Cod();
		String[] codificado= new String[bits_par.length + bits_datos.length];
		int contResult=codificado.length-1;
		int cont_comodin=0;
		
		for (int i = getFilasResultadoCod()-2; i >= 0 ; i--) {
			for (int j = comodin-1; j >= 0 ; j--) {
				if(!resultadoCod[i][j].equals("-")) {
					codificado[contResult]=""+resultadoCod[i][j];
					contResult-=1;
					cont_comodin+=1;			
				}
				
			}
			comodin=comodin-cont_comodin;
			cont_comodin=0;
		}
		
		for (int k = 0; k < getNumHamm_1Cod(); k++) {
			resultadoCod[getFilasResultadoCod()-1][k]=codificado[k];
		}	

		imprimir_Resultado(getFilasResultadoCod(),getNumHamm_1Cod(),getResultadoCod());
	}
	
	public int decodificacion() {
		bits_par= new int[paridadDeco()];//Numero Bits Paridad
		bits_datos= new int[getDatoOriginal().length()-paridadDeco()];//Cantidad de Datos
		
		resultadoDec= new String[bits_par.length+1][getDatoOriginal().length()+3];	
						
		bits_parDeco();
		bits_datoDeco();
			
		int cont_par=0;
		int cont_parCod=-1;
		int cont_datos=0;
		String []err = new String[bits_par.length];
		int aux_par=paridadDeco()-1;
		
		numerosBinarios(getDatoOriginal().length(),bits_par.length);
		
				
		for (int i = 0; i < getFilasResultadoDeco(); i++) {			
			for (int j = 0; j < getNumHamm_1Deco(); j++) {
				resultadoDec[i][j]="";				
				if(i==0) {					
					if(cont_par<paridadDeco() && j==bits_par[cont_par] ) {						
						resultadoDec[i][j]="-";
						cont_par+=1;
					}else if(j<getDatoOriginal().length()) {		
						resultadoDec[i][j]=""+bits_datos[cont_datos];
						cont_datos+=1;
					}
					else {
						resultadoDec[i][j]="-";	
					}
				}
				else{														
					if(cont_par<paridadDeco() && j==bits_par[cont_par] ) {
						resultadoDec[i][j]="-";											
						cont_par+=1;
					}else if (j<getDatoOriginal().length()) {
						String[] auxBin = binarios[j];					 
						if(auxBin[aux_par].equals("1"))
						{				
							resultadoDec[i][j]=""+bits_datos[cont_datos];		
						}else {
							resultadoDec[i][j]="-";							
						}	
						cont_datos+=1;	
					}else {
						if(i>0 && XOR(resultadoDec[i],getDatoOriginal().length())){
							//PAR
							resultadoDec[i][bits_par[i-1]]="0";
						}else if(i>0){
							//IMPAR
							resultadoDec[i][bits_par[i-1]]="1";
						}
						
						String []parCod = new String[getDatoOriginal().length()];
						arrayParCod(parCod);

						if(j==getDatoOriginal().length()){
							resultadoDec[i][j]=resultadoDec[i][bits_par[i-1]];
						}else if(j==getDatoOriginal().length()+1) {
							resultadoDec[i][j]=parCod[(int) (Math.pow(2, cont_parCod)-1)];
						}else {
							if(resultadoDec[i][j-2].equals(resultadoDec[i][j-1])) {
								resultadoDec[i][j]="0";
							}else {
								resultadoDec[i][j]="1";
							}
							err[cont_parCod]=resultadoDec[i][j];							
						}
					}					
				}
			}						
			cont_par=0;
			cont_datos=0;
			cont_parCod+=1;	
			
			if(i>0) {
				aux_par-=1;
			}

		}
		
		imprimir_Resultado(getFilasResultadoDeco(),getNumHamm_1Deco(),getResultadoDeco());
		
        int errCont=0;
        int errorConvertido=0;
        
        for (int i=err.length-1;i>=0;i--){
        	if(err[i].equals("1")) {
        		errCont+=1;
        		errorConvertido+=(int)Math.pow(2, i);
        	}	
        }
        
		if(errCont==0){
			return errCont;
		}else {
			return (int)errorConvertido;
		}
		
		
	}
	
//LLena Array con los bits de Datos de CODIFICACION	
	public void bits_datoCod() {
		String temp[]= new String[getDatoOriginal().length()];
		for (int i = 0; i < getNumHamm_2(); i++) {	
				char aux=getDatoOriginal().charAt(i);
				temp[i]=aux+"";
				int dat = Integer.parseInt(temp[i]);
				bits_datos[i]=dat;
			}		
	}
//LLena Array con los lugares donde van los bits de Paridad de CODIFICACION	
	public void bits_parCod() {
		int cont_b=0;
		int tam=getDatoOriginal().length()+cant_paridad();
		for (int i = 0; i <tam; i++) {	
			if(i==Math.pow(2, cont_b)-1){
				bits_par[cont_b]=i;
				cont_b+=1;
			}		
			
		}
	}

//LLena Array con los bits de Datos de DECODIFICACION	
	public void bits_datoDeco() {
		String temp[]= new String[getDatoOriginal().length()];
		int cont_b=0;
		int cont_d=0;
		for (int i = 0; i < getDatoOriginal().length(); i++) {
				if(cont_b<bits_par.length && i!=bits_par[cont_b]){
					char aux=getDatoOriginal().charAt(i);
					temp[i]=aux+"";
					int dat = Integer.parseInt(temp[i]);
					bits_datos[cont_d]=dat;
					cont_d+=1;
				}else if(i>=bits_par.length && cont_b>=bits_par.length) {
					char aux=getDatoOriginal().charAt(i);
					temp[i]=aux+"";
					int dat = Integer.parseInt(temp[i]);
					bits_datos[cont_d]=dat;
					cont_d+=1;
				}
				else {
					cont_b+=1;
				}
			}		
	}
//LLena Array con los lugares donde van los bits de Paridad de DECODIFICACION			
	public void bits_parDeco() {
		int cont_b=0;
		int tam=getDatoOriginal().length();
		for (int i = 0; i <tam; i++) {	
			if(i==Math.pow(2, cont_b)-1){
				bits_par[cont_b]=i;
				cont_b+=1;
			}		
			
		}
	}
	
	public void arrayParCod(String []parCod) {		
        for (int i=0;i<getDatoOriginal().length();i++){
        	char aux=getDatoOriginal().charAt(i);
        	parCod[i]=aux+"";
        }
	}
	
	public int cant_paridad() {
		
		int i=1;
		int exponente=getDatoOriginal().length();
		int temp=(int) Math.pow(2, i)+exponente;
							
		for(i=2;temp>Math.pow(2, i)-1;i++) {
			temp+=1;
		}
		return i;
		
	}
	
	public int paridadDeco() {
		
		int i=2;
		int total=getDatoOriginal().length();
		//int temp=(int) Math.pow(2, i)+exponente;
							
		for(i=2;total>Math.pow(2, i)-1;i++) {
			//total+=1;
		}
		return i;
		
	}
	
	public boolean XOR(String[] resultado2,int datoLen) {
		int cont=0;
		for(int i=0;i<datoLen;i++){
			if(resultado2[i].equals("1")) {
				cont+=1;
			}
		}
		if(cont%2==0) 
			return true;
		else
			return false;
		
	}
	
	public String errorXOR(String par_deco,String par_dco) {
		int cont=0;
		for(int i=0;i<1;i++){
			if(par_deco.equals("1")) {
				cont+=1;
			}
		}
		if(cont%2==0) 
			return "";
		else
			return "";
		
	}
	
	public boolean paridad() {
		int temp=0;
		for(int i=0;i<getNumHamm_2();i++){
			if(datoOriginal.charAt(i)=='1') {
				temp+=1;
			}
		}
		if(temp%2==0) 
			return true;
		else
			return false;
		
	}
	
	public String getDatoOriginal() {
		return datoOriginal;
	}
	
	public int getNumHamm_1Cod() {
		return resultadoCod[0].length;
	}
	
	public int getNumHamm_1Deco() {
		return resultadoDec[0].length;
	}
	
	public int getNumHamm_2() {
		return datoOriginal.length();
	}
	
	public int getFilasResultadoCod() {
		return resultadoCod.length;
	}
	
	public int getFilasResultadoDeco() {
		return resultadoDec.length;
	}

	public String[][] getResultadoCod() {
		return resultadoCod;
	}
	
	public String[][] getResultadoDeco() {
		return resultadoDec;
	}

	public int getNumBinarios() {
		return binarios.length;
	}

	public String getInfo(){
		String info="";
        
        for (int i=0;i<bits_datos.length;i++){       	     		
        	info = info +(bits_datos[i]);
        }
        return info;
	}

}
