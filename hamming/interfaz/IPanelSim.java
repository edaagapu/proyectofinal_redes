package hamming.interfaz;

import java.io.*;
import java.awt.*;
import java.awt.List;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.LineBorder;

import hamming.logica.Algoritmo;

import java.util.*;

public class IPanelSim extends UIBPanelInfo {

	private ArrayList<JTextField> matriz = new ArrayList<JTextField>();
	private Algoritmo ham;
	private JTextField txtCodigo;
	private JLabel lblDato, lblInfo, lblParidad, lblCantDatos, lblResultado, lblError;
	private int contBorrarFil = 0;
	private int contBorrarCol = 0;
	public static final String CODIFICAR = "Codificar";
	public static final String DECODIFICAR = "Decodificar";
	private JScrollPane scrollPane = new JScrollPane();

	@Override
	public void addUIControls() {

		infoUI = new JPanel();
		infoUI.setBackground(Color.WHITE);
		infoUI.setLayout(null);

		infoUI.setBounds(2, 2, 750, 555);
		// infoUI.setPreferredSize(new Dimension(750, 555));
		infoUI.setBorder(new LineBorder(new Color(0, 168, 45)));

		JLabel lblDescrInf = new JLabel("");
		lblDescrInf.setBounds(125, 5, 700, 40);
		String txtDescrInfo = "<html>"
				+ "<font color=\"#005FC2\" size=\"6\" ><b> SIMULACION DE CODIFICACION HAMMING </b></font>" + "</html>";
		lblDescrInf.setText(txtDescrInfo);
		infoUI.add(lblDescrInf);

		JLabel lblInsert = new JLabel("");
		lblInsert.setBounds(125, 57, 400, 25);
		String strInsert = "<html><color=\"BLACK\" font size=\"7\" face=\"Comic Sans MS,arial\" ><b>"
				+ "Ingrese el mensaje que desea codificar o decodificar :" + "</b></font><html>";
		lblInsert.setText(strInsert);
		infoUI.add(lblInsert);

		txtCodigo = new JTextField("");
		txtCodigo.setBounds(465, 60, 160, 25);
		infoUI.add(txtCodigo);

		lblDato = new JLabel();
		lblDato.setBounds(250, 135, 400, 25);
		infoUI.add(lblDato);

		lblInfo = new JLabel();
		lblInfo.setBounds(200, 160, 400, 25);
		infoUI.add(lblInfo);

		lblParidad = new JLabel();
		lblParidad.setBounds(250, 185, 250, 25);
		infoUI.add(lblParidad);

		lblCantDatos = new JLabel();
		lblCantDatos.setBounds(150, 210, 600, 25);
		infoUI.add(lblCantDatos);

		lblResultado = new JLabel();
		lblResultado.setBounds(200, 490, 600, 25);
		infoUI.add(lblResultado);

		lblError = new JLabel();
		lblError.setBounds(200, 510, 600, 25);
		infoUI.add(lblError);

		scrollPane.setBounds(75, 250, 600, 200);
		scrollPane.setBorder(new LineBorder(Color.BLACK));

		JButton codificar = new JButton(IPanelSim.CODIFICAR);
		codificar.setBounds(175, 100, 160, 25);
		codificar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				borrar_hamming();

				String cod = txtCodigo.getText();

				ham = new Algoritmo(cod);
				ham.codificacion();
				int fil = ham.getFilasResultadoCod() + 2;
				int col = ham.getNumHamm_1Cod() + 1;

				contBorrarFil = fil;
				contBorrarCol = col;
				scrollPane.setViewportView(
						tabla_txtF(fil, col, 55, 25, ham.cant_paridad(), ham.getNumHamm_2(), IPanelSim.CODIFICAR));

				llenarDatos(ham.getInfo(), ham.cant_paridad(), ham.getNumHamm_1Cod(), ham.getResultadoCod(), -1);
				infoUI.add(scrollPane);
			}
		});
		infoUI.add(codificar);

		JButton decodificar = new JButton(IPanelSim.DECODIFICAR);
		decodificar.setBounds(420, 100, 160, 25);
		decodificar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				borrar_hamming();

				String cod = txtCodigo.getText();

				ham = new Algoritmo(cod);
				int datoError = ham.decodificacion();
				int fil = ham.getFilasResultadoDeco() + 2;
				int col = ham.getNumHamm_1Deco() + 1;

				contBorrarFil = fil;
				contBorrarCol = col;

				scrollPane.setViewportView(
						tabla_txtF(fil, col, 55, 25, ham.paridadDeco(), ham.getNumHamm_2(), IPanelSim.DECODIFICAR));

				llenarDatos(ham.getInfo(), ham.paridadDeco(), ham.getNumHamm_1Deco(), ham.getResultadoDeco(),
						datoError);
				infoUI.add(scrollPane);
			}
		});
		infoUI.add(decodificar);

	}

	@Override
	public void initialize() {

	}

	public void borrar_hamming() {
		if (contBorrarFil > 0 || contBorrarCol > 0) {
			int contMat = 0;
			for (int i = 0; i < contBorrarFil; i++) {
				for (int j = 0; j < contBorrarCol; j++) {
					matriz.get(contMat).removeAll();
					infoUI.remove(matriz.get(contMat));
					contMat += 1;
				}
				infoUI.repaint();
				scrollPane.setViewportView(null);
			}
			matriz.removeAll(matriz);
		}

	}

	public void llenarDatos(String info, int paridad, int totalHam, String[][] resultHam, int bitError) {

		String strDato = "<html><font color=\"#005FC2\" size=\"4\" face=\"Comic Sans MS,arial\" ><b>"
				+ "Dato Original Ingresado:  "
				+ "</font><font color=\"RED\" size=\"4\" face=\"Comic Sans MS,arial\" > &nbsp;" + txtCodigo.getText()
				+ "</b></font><html>";
		lblDato.setText(strDato);

		String strInfo = "<html><font color=\"#005FC2\" size=\"4\" face=\"Comic Sans MS,arial\" ><b>"
				+ "Informaci�n o datos:  "
				+ "</font><font color=\"RED\" size=\"4\" face=\"Comic Sans MS,arial\" > &nbsp;" + info
				+ "</b></font><html>";
		lblInfo.setText(strInfo);

		String strParidad = "<html><font color=\"#005FC2\" size=\"4\" face=\"Comic Sans MS,arial\" ><b>"
				+ "Numero de Bits de Paridad:  "
				+ "</font><font color=\"RED\" size=\"4\" face=\"Comic Sans MS,arial\" > &nbsp;" + paridad
				+ "</b></font><html>";
		lblParidad.setText(strParidad);

		String tipo = "";
		int cantDatos = 0;

		if (info.equals(txtCodigo.getText())) {
			cantDatos = totalHam;
			tipo = "Codificar";
		} else {
			cantDatos = totalHam - 3;
			tipo = "Decodificar";
		}

		String strCantDatos = "<html><font color=\"#005FC2\" size=\"4\" face=\"Comic Sans MS,arial\" ><b>"
				+ "Tama�o de la palabra a " + tipo + " : "
				+ "</font><font color=\"RED\" size=\"4\" face=\"Comic Sans MS,arial\" > &nbsp;" + (cantDatos)
				+ "</font><font color=\"#00A82D\" size=\"4\" face=\"Comic Sans MS,arial\" > &nbsp; "
				+ "&nbsp; Hamming: &nbsp; " + " H ( " + (cantDatos) + " , " + (info.length()) + " ) "
				+ " </b></font><html>";
		lblCantDatos.setText(strCantDatos);

		String res = "";

		for (int i = 0; i < totalHam; i++) {
			if (info.equals(txtCodigo.getText())) {
				res = res + (resultHam[paridad + 1][i]);
			} else {
				res = "" + info;
			}

		}

		String strResult = "<html><font color=\"#005FC2\" size=\"4\" face=\"Comic Sans MS,arial\" ><b>"
				+ "&nbsp; Hamming: &nbsp; " + " H ( " + (cantDatos) + " , " + (info.length()) + " ) = "
				+ "</font><font color=\"RED\" size=\"4\" face=\"Comic Sans MS,arial\" > &nbsp; " + res
				+ " </b></font><html>";
		lblResultado.setText(strResult);

		if (bitError == -1) {
			System.out.println("NO ERROR");
			lblError.setText("");
		} else if (bitError == 0) {
			String strError = "<html><font color=\"#005FC2\" size=\"4\" face=\"Comic Sans MS,arial\" ><b>"
					+ "&nbsp; No hubo errores en la Decodificaci�n " + " </b></font><html>";
			lblError.setText(strError);
		} else {
			String strError = "<html><font color=\"#005FC2\" size=\"4\" face=\"Comic Sans MS,arial\" ><b>"
					+ "&nbsp; Error en dato original en el bit: &nbsp; "
					+ "</font><font color=\"RED\" size=\"4\" face=\"Comic Sans MS,arial\" > &nbsp; " + bitError
					+ " </b></font><html>";
			lblError.setText(strError);
		}

		txtCodigo.setText("");
	}

	public JPanel tabla_txtF(int filas, int columnas, int tam_x, int tam_y, int n_paridad, int n_datos, String tipo) {

		int cont_paridad = 1;
		int cont_paridad_hor = 0;
		int cont_datos = 1;
		int cont_result_X = 0;
		int cont_result_Y = -1;

		JPanel pnlTabla = new JPanel();
		pnlTabla.setPreferredSize(new Dimension((columnas * tam_x) + 50, 50 + (tam_y * filas)));
		pnlTabla.setLayout(null);
		pnlTabla.setBackground(Color.WHITE);
		for (int i = 0; i < filas; i++) {

			for (int j = 0; j < columnas; j++) {
				// g.setColor(new Color(0,145,124));
				JTextField txt_temp = new JTextField();
				txt_temp.setText("");
				// Code adding the component to the parent container - not shown here
				if (i == 0) {
					if (j == 0) {
						txt_temp.setText("Posici�n");
						txt_temp.setBounds(35 + tam_x * j, 35 + tam_y * i, tam_x + 5, tam_y);
					} else if (tipo.equals(IPanelSim.CODIFICAR)) {
						String binario = Long.toBinaryString(j);
						txt_temp.setText(binario + " (" + j + ")");
						txt_temp.setBounds(35 + tam_x * j, 35 + tam_y * i, tam_x, tam_y);
					} else if (j <= ham.getNumBinarios()) {
						String binario = Long.toBinaryString(j);
						txt_temp.setText(binario + " (" + j + ")");
						txt_temp.setBounds(35 + tam_x * j, 35 + tam_y * i, tam_x, tam_y);
					} else if (j == ham.getNumBinarios() + 1) {
						txt_temp.setText("Par Dec");
						txt_temp.setBounds(30 + tam_x * j, 35 + tam_y * i, tam_x + 5, tam_y);
					} else if (j == ham.getNumBinarios() + 2) {
						txt_temp.setText("Par Cod");
						txt_temp.setBounds(30 + tam_x * j, 35 + tam_y * i, tam_x + 5, tam_y);
					} else {
						txt_temp.setText("Calc Err");
						txt_temp.setBounds(30 + tam_x * j, 35 + tam_y * i, tam_x + 5, tam_y);
					}
					txt_temp.setBackground(new Color(0, 168, 45));
					txt_temp.setBorder(BorderFactory.createLineBorder(new Color(0, 168, 45), 1));
					txt_temp.setForeground(Color.WHITE);
				} else if (i == 1) {

					if (j == Math.pow(2, cont_paridad_hor) && j <= ham.getNumBinarios()) {
						cont_paridad_hor += 1;
						txt_temp.setText("P" + cont_paridad_hor);
					} else if (j > 0 && j <= ham.getNumBinarios()) {
						txt_temp.setText("D" + cont_datos);
						cont_datos += 1;
					} else {
						txt_temp.setText(" ");
					}
					txt_temp.setBackground(new Color(0, 95, 194));
					txt_temp.setBounds(35 + tam_x * j, 35 + tam_y * i, tam_x, tam_y);
					txt_temp.setBorder(BorderFactory.createLineBorder(new Color(0, 95, 194), 1));
					txt_temp.setForeground(Color.WHITE);

				} else if (j == 0 && i > 1) {
					if (i == 2) {
						txt_temp.setText("Datos");
					} else if (i != filas - 1) {
						txt_temp.setText("P" + cont_paridad);
						cont_paridad += 1;
					} else if (tipo == IPanelSim.CODIFICAR) {
						txt_temp.setText("H ( " + (n_paridad + n_datos) + " , " + n_datos + " )");
					} else {
						txt_temp.setText("P" + cont_paridad);
						cont_paridad += 1;
					}
					txt_temp.setBackground(Color.WHITE);
					txt_temp.setBorder(BorderFactory.createLineBorder(Color.WHITE, 1));
					txt_temp.setBounds(35 + tam_x * j, 35 + tam_y * i, tam_x + 5, tam_y);
				} else {

					txt_temp.setBackground(Color.WHITE);
					txt_temp.setBorder(BorderFactory.createLineBorder(Color.WHITE, 1));
					txt_temp.setBounds(35 + tam_x * j, 35 + tam_y * i, tam_x, tam_y);

					// System.out.println("ENTRO MATRIZ INICIO \n");
					if (tipo == IPanelSim.CODIFICAR) {
						txt_temp.setText(ham.getResultadoCod()[cont_result_X][cont_result_Y]);
					} else {
						txt_temp.setText(ham.getResultadoDeco()[cont_result_X][cont_result_Y]);
					}
					cont_result_Y += 1;

				}
				txt_temp.setHorizontalAlignment(JTextField.CENTER);
				txt_temp.setEditable(false);
				matriz.add(txt_temp);
				pnlTabla.add(txt_temp);
			}
			// cont_paridad=0;
			if (i > 1) {
				cont_result_X += 1;
			}
			cont_result_Y = 0;
		}
		return pnlTabla;
	}

}
