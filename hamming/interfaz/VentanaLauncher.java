package hamming.interfaz;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.border.EmptyBorder;

public class VentanaLauncher extends JFrame {

	 private JPanel contentPane;	 
	 
	 public VentanaLauncher() {
		 
	        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	        setBounds(800, 320, 400, 400);
	        contentPane = new JPanel();
	        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
	        setContentPane(contentPane);
	        contentPane.setLayout(null);
	              
	        
	        JLabel lblHoja = new JLabel("Menu INICIO");
	        lblHoja.setBounds(46, 26, 146, 14);
	        contentPane.add(lblHoja);
	        	       	     
	        
	        
	        JButton btn_hamming = new JButton("Codificación Hamming");
	        btn_hamming.addActionListener(new ActionListener() {
	            public void actionPerformed(ActionEvent arg0) {
	                System.out.println("Si");
	                
	                try {
	                    VentanaPriHamm frame = new VentanaPriHamm();
	                    frame.setVisible(true);	                    
	                } catch (Exception e) {
	                    e.printStackTrace();
	                }
	            }
	        });
	        btn_hamming.setBounds(85, 100, 210, 24);
	        contentPane.add(btn_hamming);
	        
	        JButton btn_shanonFano = new JButton("Codificación Shannon-Fano");
	        btn_shanonFano.addActionListener(new ActionListener() {
	            public void actionPerformed(ActionEvent arg0) {
	                System.out.println("Si");
	            }
	        });
	        btn_shanonFano.setBounds(85, 175, 210, 24);
	        contentPane.add(btn_shanonFano);
	        
	        JButton btn_manchester = new JButton("Codificación Manchester");
	        btn_manchester.addActionListener(new ActionListener() {
	            public void actionPerformed(ActionEvent arg0) {
	                System.out.println("Si");
	            }
	        });
	        btn_manchester.setBounds(85, 250, 210, 24);
	        contentPane.add(btn_manchester);
	        
	    }
	
	
}
