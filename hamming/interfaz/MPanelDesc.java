package hamming.interfaz;

import java.io.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import java.util.*;

public class MPanelDesc extends UIBPanelMenu{

	@Override
	public void addUIControls() {

		menuUI = new JPanel();
		menuUI.setBackground(Color.WHITE);
		menuUI.setLayout (null);
		
		menuUI.setBounds(2, 2, 510, 155);
		menuUI.setBorder(new LineBorder(new Color(0,95,194)));
		
               
        JLabel lblMenuQ = new JLabel("<html><font color=\"red\"><b> �QUE ES HAMMING? </font></html>");
        lblMenuQ.setBounds(210, 5, 180, 15);
        menuUI.add(lblMenuQ);
        
        JLabel lbl_Desc = new JLabel("");
        lbl_Desc.setBounds(10, 25, 490, 125);
         
        String txtDesc="<html>"
        		+"Parece un sueño que si enviamos unos bits (una imagen, un audio,"
        		+ " un texto, lo que sea) por Internet. Cuando llegue a su destino,"
        		+ " y llegue mal (bits cambiados aleatoriamente; donde antes había "
        		+ "un �1� ahora un �0�, y viceversa) por alguna causa -como una tormenta"
        		+ " electromagnética por el camino entre el ordenador que lo envía y el"
        		+ " que lo recibe- podamos corregir los errores que han ocurrido por el "
        		+ "camino en el destino.<p>" 
        		+"<i>Despues de leer este pequeño parrafo introductorio, para "
        		+"encontrar mas información más detallada,puede mirar "
        		+" en el panel de abajo. </i>  "
        		+"</html>";
        lbl_Desc.setText(txtDesc);
        menuUI.add(lbl_Desc);
		
		
	}

	@Override
	public void initialize() {
		
	}
	
	public void paint(Graphics g)
    {
        paint(g);	      
    }

}
