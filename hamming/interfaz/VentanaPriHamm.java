package hamming.interfaz;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import interfaz.VMenu;

public class VentanaPriHamm extends JFrame {

	public static final String DESCRIPCION = "Descripci�n";
	public static final String EJEMPLO = "Ejemplo";
	public static final String SIMULACION = "Simulacion";

	public static final String BACK = "Volver";
	public static final String EXIT = "Salir";

	private JPanel contentPane;
	private JPanel panel_Menu;
	private JPanel panel_Info;
	private JLabel lbltablita;
	private JButton btn_desc;
	private JButton btn_ejem;
	private JButton btn_sim;
	private JScrollPane scrollPane;

	public VentanaPriHamm() {

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		setBounds(500, 10, 800, 700);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);
		contentPane.setBackground(Color.WHITE);

		panel_Menu = new JPanel();
		panel_Menu.setBounds(15, 15, 515, 160);
		panel_Menu.setBorder(new LineBorder(new Color(0, 95, 194)));
		panel_Menu.setLayout(null);
		panel_Menu.setBackground(Color.WHITE);

		scrollPane = new JScrollPane();
		scrollPane.setBounds(15, 185, 755, 450);
		panel_Info = new JPanel();
		panel_Info.setBackground(Color.WHITE);
		panel_Info.setPreferredSize(new Dimension(755, 560));
		panel_Info.setBorder(new LineBorder(new Color(0, 168, 45)));
		panel_Info.setLayout(null);

		scrollPane.setViewportView(panel_Info);
		contentPane.add(scrollPane);

		JLabel lbl_TxtMenu = new JLabel("Text Menu");
		lbl_TxtMenu.setBounds(15, 15, 500, 130);
		String txtTitle = "<html>" + "<font color=\"RED\" size=\"18\" ><b> "
				+ "C O D I F I C A C I O N <p> &emsp; &emsp; &emsp; &emsp; &emsp;  H A M M I N G "
				+ "</b></font></html>";
		lbl_TxtMenu.setText(txtTitle);

		lbltablita = new JLabel();
		lbltablita.setBounds(50, -30, 700, 600);
		String txtIni = "<html> " + "<center><font color=\"#005FC2\" size=\"10\" ><b> BIENVENIDO </b></font></center>"
				+ "<font size=\"6\" face=\"Comic Sans MS,arial\" ><b>" + "<p><p> "
				+ "En este programa se va a explicar todo acerca de la codificacion hamming. "
				+ "A continuaci�n, en el menu de botones podra escoger entre opciones como: <p> <p>"
				+ "<font color=\"#00A82D\"><b>Descripci�n:</b></font> Le permitira saber la informaci�n "
				+ " necesaria para entender el codigo hamming.<p><p>"
				+ "<font color=\"#00A82D\"><b>Ejemplo:</b></font> Una explicaci�n practica y visual de "
				+ "como funciona el algoritmo con un codigo de prueba.<p><p>"
				+ "<font color=\"#00A82D\"><b>Simulaci�n:</b></font> Se mostrara el programa con el formato "
				+ "para ejecutar el algoritmo y obtener su resultado." + "</b></font></html>";
		lbltablita.setText(txtIni);

		buttonHandler vf = new buttonHandler(this);

		JPanel panel_button = new JPanel();
		panel_button.setBounds(540, 15, 230, 160);
		panel_button.setLayout(new GridLayout(5, 1));
		panel_button.setBorder(new LineBorder(Color.GRAY));

		btn_desc = new JButton(VentanaPriHamm.DESCRIPCION);
		// btn_desc.setBounds(15,20,180, 30);
		btn_desc.addActionListener(vf);
		btn_desc.setBackground(Color.WHITE);

		btn_ejem = new JButton(VentanaPriHamm.EJEMPLO);
		// btn_ejem.setBounds(15,60,180, 30);
		btn_ejem.addActionListener(vf);
		btn_ejem.setBackground(Color.WHITE);

		btn_sim = new JButton(VentanaPriHamm.SIMULACION);
		// btn_sim.setBounds(15,100,180, 30);
		btn_sim.setBackground(Color.WHITE);
		btn_sim.addActionListener(vf);

		JButton btnBack = new JButton(VentanaPriHamm.BACK);
		btnBack.setBackground(Color.WHITE);
		btnBack.addActionListener(vf);

		JButton btnExit = new JButton(VentanaPriHamm.EXIT);
		btnExit.setMnemonic(KeyEvent.VK_X);
		btnExit.setBackground(Color.WHITE);
		btnExit.addActionListener(vf);

		panel_Menu.add(lbl_TxtMenu);
		panel_Info.add(lbltablita);
		panel_button.add(btn_desc);
		panel_button.add(btn_ejem);
		panel_button.add(btn_sim);
		panel_button.add(btnBack);
		panel_button.add(btnExit);

		contentPane.add(panel_button);
		contentPane.add(panel_Menu);
//		contentPane.add(panel_Info);

		setContentPane(contentPane);
	}

	public void displayNewUI(JPanel panel_M, JPanel panel_I) {
		panel_Menu.removeAll();
		panel_Menu.add(panel_M);
		panel_Menu.revalidate();

		panel_Info.removeAll();
		panel_Info.add(panel_I);
		panel_Info.revalidate();
		panel_Info.repaint();

		contentPane.add(panel_Menu);
		// contentPane.add(panel_Info);
		scrollPane.setViewportView(panel_Info);
		setContentPane(contentPane);

	}

	public String getBtn_desc() {
		return (String) btn_desc.getText();
	}

	public String getBtn_ejem() {
		return (String) btn_ejem.getText();
	}

	public String getBtn_sim() {
		return (String) btn_sim.getText();
	}
}

class buttonHandler implements ActionListener {
	VentanaPriHamm manager;
	UIBPanelMenu builder_M;
	UIBPanelInfo builder_I;

	public void actionPerformed(ActionEvent e) {

		if (e.getActionCommand().equals(VentanaPriHamm.EXIT)) {
			System.exit(1);
		}
		if (e.getActionCommand().equals(VentanaPriHamm.DESCRIPCION)
				|| e.getActionCommand().equals(VentanaPriHamm.EJEMPLO)
				|| e.getActionCommand().equals(VentanaPriHamm.SIMULACION)) {

			BuilderFactory factory = new BuilderFactory();
			// create an appropriate builder instance
			if (e.getActionCommand().equals(VentanaPriHamm.DESCRIPCION)) {
				builder_M = factory.getUIBPanelMenu(manager.getBtn_desc());
				builder_I = factory.getUIBPanelInfo(manager.getBtn_desc());
			} else if (e.getActionCommand().equals(VentanaPriHamm.EJEMPLO)) {
				builder_M = factory.getUIBPanelMenu(manager.getBtn_ejem());
				builder_I = factory.getUIBPanelInfo(manager.getBtn_ejem());
			} else if (e.getActionCommand().equals(VentanaPriHamm.SIMULACION)) {
				builder_M = factory.getUIBPanelMenu(manager.getBtn_sim());
				builder_I = factory.getUIBPanelInfo(manager.getBtn_sim());
			}

			// configure the director with the builder
			UIDirectorPanels director = new UIDirectorPanels(builder_M, builder_I);
			// director invokes different builder
			// methods
			director.build();
			// get the final build object
			JPanel UIMenuObj = builder_M.getMenuUI();
			JPanel UIInfoObj = builder_I.getInfoUI();
			manager.displayNewUI(UIMenuObj, UIInfoObj);

		}
		if (e.getActionCommand().equals(VentanaPriHamm.BACK)) {
			VMenu menu = new VMenu();
			menu.setVisible(true);
			manager.dispose();
		}

	}

	public buttonHandler() {
	}

	public buttonHandler(VentanaPriHamm inManager) {
		manager = inManager;
	}
}

class BuilderFactory {
	public UIBPanelMenu getUIBPanelMenu(String str) {
		UIBPanelMenu builder_M = null;
		if (str.equals(VentanaPriHamm.DESCRIPCION)) {
			builder_M = new MPanelDesc();
		} else if (str.equals(VentanaPriHamm.EJEMPLO)) {
			builder_M = new MPanelEjem();
		} else if (str.equals(VentanaPriHamm.SIMULACION)) {
			builder_M = new MPanelSim();
		}
		return builder_M;
	}

	public UIBPanelInfo getUIBPanelInfo(String str) {
		UIBPanelInfo builder_I = null;
		if (str.equals(VentanaPriHamm.DESCRIPCION)) {
			builder_I = new IPanelDesc();
		} else if (str.equals(VentanaPriHamm.EJEMPLO)) {
			builder_I = new IPanelEjem();
		} else if (str.equals(VentanaPriHamm.SIMULACION)) {
			builder_I = new IPanelSim();
		}
		return builder_I;
	}
}
