package hamming.interfaz;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public abstract class UIBPanelMenu {
	 protected JPanel menuUI;

	 public abstract void addUIControls();
	 public abstract void initialize();
	 //public abstract String getSQL();

	 public JPanel getMenuUI() {
	   return menuUI; 
	 }
	public abstract void paint(Graphics g);
}
