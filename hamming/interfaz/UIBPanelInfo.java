package hamming.interfaz;


import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public abstract class UIBPanelInfo {
	protected JPanel infoUI;

	 public abstract void addUIControls();
	 public abstract void initialize();
	 //public abstract String getSQL();

	 public JPanel getInfoUI() {
	   return infoUI;
	 }
	 
}
