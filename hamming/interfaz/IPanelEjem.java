package hamming.interfaz;

import java.io.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.LineBorder;

import hamming.logica.Algoritmo;

import java.util.*;

public class IPanelEjem extends UIBPanelInfo {

	public static final String CODIFICAR = "Codificar";
	public static final String DECODIFICAR = "Decodificar";
	public static final String SIGUIENTE = "Siguiente";
	private int paso;
	private JButton btnCod, btnDeco;
	private JLabel imagenEjem, lblEjemplo;
	private ImageIcon i_labelEjem;
	private Icon iconoEjem;
	private String url = "/hamming/imagenes/ejem";
	private String control;

	@Override
	public void addUIControls() {

		infoUI = new JPanel();
		infoUI.setBackground(Color.WHITE);
		infoUI.setLayout(null);

		infoUI.setBounds(2, 2, 750, 555);
		infoUI.setBorder(new LineBorder(new Color(0, 168, 45)));

		JLabel lblDescrInf = new JLabel("");
		lblDescrInf.setBounds(210, 5, 545, 55);
		String txtInfo = "<html>" + "<font color=\"#005FC2\" size=\"20\" ><b> EJEMPLO HAMMING </b></font>" + "</html>";
		lblDescrInf.setText(txtInfo);
		infoUI.add(lblDescrInf);

		JLabel lblOpcion = new JLabel("");
		lblOpcion.setBounds(275, 60, 400, 25);
		String strOpcion = "<html><color=\"BLACK\" font size=\"7\" face=\"Comic Sans MS,arial\" ><b>"
				+ "Escoja el ejemplo que desea observar : " + "</b></font><html>";
		lblOpcion.setText(strOpcion);
		infoUI.add(lblOpcion);

		lblEjemplo = new JLabel("");
		lblEjemplo.setBounds(25, 15, 700, 340);
		infoUI.add(lblEjemplo);

		imagenEjem = new JLabel();
		// imagenEjem.setBounds(150,250,440,180);
		// imagen.setBounds(600,50,230,339);

		infoUI.add(imagenEjem);

		btnCod = new JButton(IPanelEjem.CODIFICAR);
		btnCod.setBounds(175, 100, 160, 25);
		infoUI.add(btnCod);

		btnDeco = new JButton(IPanelEjem.DECODIFICAR);
		btnDeco.setBounds(420, 100, 160, 25);
		infoUI.add(btnDeco);

		JButton btnSiguiente = new JButton(IPanelEjem.SIGUIENTE);
		btnSiguiente.setBounds(300, 500, 160, 25);

		btnCod.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				paso = 0;
				control = IPanelEjem.CODIFICAR;
				btnSiguiente.setVisible(true);

				String strEjem = "<html><font color=\"#000000\" size=\"3\" face=\"Comic Sans MS,arial\" ><b>"
						+ "Cuando se realiza la codificacion se tiene en cuenta que se evalua con base a un numero "
						+ "binario ingresado por el usuario y se tendran principalmente 4 cosas en cuenta: <p><p> "
						+ "1. Evaluar los Bits de paridad, que dependeran del binario a codificar, estos,  "
						+ "son los P(N�) vistos en la tabla y corresponden a las potencias de 2: 2^k.  <p>"
						+ "2. Se procede a colocar los datos en la priemra columna en las casillas D(N�) "
						+ " que corresponden a cada digito del numero ingresado ubicado en dicha columna. "
						+ "El orden de los datos es de Izquierda a Derecha.<p>"
						+ "3. Evaluando el algoritmo se bajan los bits a la siguiente Fila, que dependera de si, "
						+ "el numero de la paridad en la fila, evaluar si en las casillas D, el numero  "
						+ "correspondiente en binario, posee un 1 en la posicion P(N�) evaluan de Derecha a Izquierda. <p>"
						+ "4. Hacer los mismos pasos hasta el ultimo P(n) en las filas y en la ultima fila bajar  "
						+ "el ultimo valor de cada columna. <p> <p>"
						+ "A continuaci�n se muestra el ejemplo paso a paso: El numero ingresado es: 1001"
						+ "</b></font><html>";
				lblEjemplo.setText(strEjem);

				changeImage(".jpg");

				if (paso != 5) {
					btnCod.setVisible(false);
					btnDeco.setVisible(false);
					lblOpcion.setVisible(false);
				}

			}
		});

		btnDeco.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				paso = 0;
				control = IPanelEjem.DECODIFICAR;
				btnSiguiente.setBounds(300, 525, 160, 25);
				lblEjemplo.setBounds(25, 20, 700, 340);
				btnSiguiente.setVisible(true);

				String strEjem = "<html><font color=\"#000000\" size=\"3\" face=\"Comic Sans MS,arial\" ><b>"
						+ "La decodificacion  se evalua con base en el mismo "
						+ "algoritmo de la codificacion, pero con unas ligeras variantes, teniendo en cuenta "
						+ "que el numero viene ahora con los datos y su respectiva paridad, respetando sus posiciones, "
						+ "para esto, se tienen encuenta igualmente 4 items, adem�s, de contemplar un posibles error "
						+ "dando cambio de un digito en cualquier posici�n. A continuacion a tener en cuenta: <p> <p>"
						+ "1. Evaluar los Bits de paridad, que se encuentran en el binario codificado, estos,  "
						+ "se agregan en una de las 3 nuevas columnas que corresponde a la de nombre \"Par Cod\". "
						+ "Para la columna \"Par Dec\" se iran poniendo los bits correspondientes a la paridad en cada Fila. <p>"
						+ "2. Se bajan los datos en la primera columna en las casillas D(N�) "
						+ " que corresponden a cada digito(Diferente a los 2^k) del numero ingresado. "
						+ "El orden de los datos es de Izquierda a Derecha.<p>"
						+ "3. Evaluando el algoritmo, hacer los mismos pasos que en la codificacion en cada fila. <p> "
						+ "4. para corroborar si hubo un Error en algun dato, se realiza un XOR entre la columna \"Par Dec\" "
						+ "y \"Par Cod\" dando esto como resultado, los valores en cada fila de la columna \"Calc Err\" "
						+ "por ultimo, si todos los valores de columna son 0, significa que no hubo error, si no, se "
						+ "evalua la columna de abajo hacia arriba en las Filas con P(N�) y se pasa el numero a decimal"
						+ "y esta sera la posici�n del error.<p>"
						+ "A continuaci�n se muestra el ejemplo paso a paso: El numero ingresado es: 0011011"
						+ "</b></font><html>";
				lblEjemplo.setText(strEjem);

				changeImage("Deco.jpg");

				if (paso != 5) {
					btnCod.setVisible(false);
					btnDeco.setVisible(false);
					lblOpcion.setVisible(false);
				}

			}
		});

		btnSiguiente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				paso += 1;

				if (control == IPanelEjem.CODIFICAR && paso != 5) {
					changeImage(".jpg");
				} else if (paso != 5) {
					changeImage("Deco.jpg");
				}

				if (paso == 5) {
					lblEjemplo.setVisible(false);
					imagenEjem.setVisible(false);
					btnSiguiente.setVisible(false);
					btnCod.setVisible(true);
					btnDeco.setVisible(true);
					lblOpcion.setVisible(true);
					btnSiguiente.setText("Siguiente");
				} else if (paso == 4) {
					btnSiguiente.setText("Terminar");
				}

			}
		});
		btnSiguiente.setVisible(false);
		infoUI.add(btnSiguiente);

	}

	@Override
	public void initialize() {
		// TODO Auto-generated method stub

	}

	public void changeImage(String nom_url) {
		imagenEjem.removeAll();
		imagenEjem.repaint();
		imagenEjem.setVisible(true);
		lblEjemplo.setVisible(true);
		// JOptionPane.showMessageDialog(null, " Borranding... ");
		if (control == IPanelEjem.CODIFICAR) {
			imagenEjem.setBounds(150, 315, 441, 52 + 26 * (paso + 1));
		} else {
			imagenEjem.setBounds(75, 335, 605, 78 + 26 * (paso));
		}

		i_labelEjem = new ImageIcon(getClass().getResource(url + (paso + 1) + nom_url));
		iconoEjem = new ImageIcon(i_labelEjem.getImage().getScaledInstance(imagenEjem.getWidth(),
				imagenEjem.getHeight(), Image.SCALE_DEFAULT));
		imagenEjem.setIcon(iconoEjem);
	}

}
