package hamming.interfaz;

import java.awt.Graphics;

public class UIDirectorPanels {

	private UIBPanelMenu builder_M;
	private UIBPanelInfo builder_I;
	
	public UIDirectorPanels(UIBPanelMenu bldr_M,UIBPanelInfo bldr_I) {
		builder_M = bldr_M;
		builder_I = bldr_I;
	}
	
	public void build() {
		builder_M.addUIControls();
		builder_M.initialize();
		
		builder_I.addUIControls();
		builder_I.initialize(); 

	}
}
