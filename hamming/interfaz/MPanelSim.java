package hamming.interfaz;

import java.io.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.LineBorder;

import java.util.*;

public class MPanelSim extends UIBPanelMenu {

	@Override
	public void addUIControls() {

		menuUI = new JPanel();
		menuUI.setBackground(Color.WHITE);
		menuUI.setLayout(null);

		menuUI.setBounds(2, 2, 510, 155);
		menuUI.setBorder(new LineBorder(new Color(0, 95, 194)));

		JLabel lblMenuQ = new JLabel("<html><font color=\"red\"><b> INSTRUCCIONES SIMULADOR HAMMING </font></html>");
		lblMenuQ.setBounds(125, 5, 380, 15);
		menuUI.add(lblMenuQ);

		JLabel lbl_Desc = new JLabel("");
		lbl_Desc.setBounds(10, 25, 495, 140);

		String txtDesc = "<html>" + "Para realizar correctamente la simulaci�n, ingrese unicamente valores binarios "
				+ "debido a que el programa y codificaci�n tratan este tipo de datos, de no ser asi, "
				+ "no tendra un resultado, ni exitoso, ni esperado al hacer uso del programa.  "
				+ "Despues de ingresar el numero tiene la opcion de codificar o decodificar, esto "
				+ "dependera si quiere enviar un archivo codificado, o quiere descifrar un mensaje "
				+ "que le enviaron, despues de pulsar el boton se mostrara el resultado de la "
				+ "simulaci�n, junto con los datos correspondientes y que son de su interes."
				+ "<i>Despues de leer este peque�o parrafo introducctorio, para "
				+ "encontrar mas informaci�n m�s detallada,puede mirar " + " en el panel de abajo. </i>  " + "</html>";
		lbl_Desc.setText(txtDesc);
		menuUI.add(lbl_Desc);
	}

	@Override
	public void initialize() {
		// TODO Auto-generated method stub

	}

	@Override
	public void paint(Graphics g) {
		// TODO Auto-generated method stub
		paint(g);
	}

}
