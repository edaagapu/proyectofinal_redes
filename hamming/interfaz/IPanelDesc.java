package hamming.interfaz;

import java.io.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import java.util.*;

public class IPanelDesc extends UIBPanelInfo {

	@Override
	public void addUIControls() {

		infoUI = new JPanel();
		infoUI.setBackground(Color.WHITE);
		infoUI.setLayout(null);

		infoUI.setBounds(2, 2, 750, 555);
		infoUI.setBorder(new LineBorder(new Color(0, 168, 45)));

		JLabel lblDescrInf = new JLabel("");
		lblDescrInf.setBounds(280, 5, 245, 55);
		String txtInfo = "<html>" + "<font color=\"#005FC2\" size=\"20\" ><b> DESCRIPCI�N </b></font>" + "</html>";
		lblDescrInf.setText(txtInfo);
		infoUI.add(lblDescrInf);

		JLabel lbl_Inf = new JLabel("");
		lbl_Inf.setBounds(15, -1, 720, 490);
		String txtInf = "<html>" + "<font size=\"4\" face=\"Comic Sans MS,arial\" ><b>"
				+ "Es un m�todo general propuesto por R. W Hamming usando\r\n"
				+ "una distancia m�nima m. Con este m�todo, por cada entero m\r\n"
				+ "existe un c�digo de hamming de 2m-1 bits que contiene m bits\r\n"
				+ "de paridad y 2^m-1-m bits de informaci�n.En este c�digo, los\r\n"
				+ "bits de paridad y los bits de paridad se encuentran\r\n"
				+ "entremezclados de la siguiente forma: Si se numeran las\r\n"
				+ "posiciones de los bits desde 1 hasta 2^m-1, los bits en la\r\n"
				+ "posici�n 2^k, donde , son los bits de paridad y los bits restantes"
				+ "son bits de informaci�n. <p><p>" + "El valor de cada bit de paridad se escoge de modo que el\r\n"
				+ "total de unos en un n�mero espec�fico de bits sea par, y estos\r\n"
				+ "grupos se escogen de tal forma que ning�n bit de informaci�n\r\n"
				+ "se cubra con la misma combinaci�n de bits de paridad. Es lo\r\n"
				+ "anterior lo que proporciona al c�digo su capacidad de\r\n" + "correcci�n. <p><p>"
				+ "Para cada bit de paridad en la posici�n 2^k, su grupo de bits\r\n"
				+ "de informaci�n correspondiente incluye todos esos bits de\r\n"
				+ "informaci�n correspondiente cuya representaci�n binaria tenga\r\n"
				+ "un uno en la posici�n  2^k. La siguiente tabla muestra los grupos\r\n"
				+ "de paridad para un c�digo de hamming de 7 bits o sea de la\r\n"
				+ "forma 2^m-1 con m = 3. En este ejemplo, los bits de\r\n"
				+ "informaci�n son 4 y los bits de paridad son 3. Los bits de\r\n"
				+ "informaci�n est�n en las posiciones 7, 6, 5 ,3. Los bits de\r\n"
				+ "paridad est�n en las posiciones 1, 2, 4." + "" + "</b></font></html>";
		lbl_Inf.setText(txtInf);
		infoUI.add(lbl_Inf);

		JLabel imagen = new JLabel();
		imagen.setBounds(300, 405, 166, 145);
		// imagen.setBounds(600,50,230,339);

		ImageIcon i_label = new ImageIcon(getClass().getResource("/hamming/imagenes/hamm2.jpg"));
		Icon icono1 = new ImageIcon(
				i_label.getImage().getScaledInstance(imagen.getWidth(), imagen.getHeight(), Image.SCALE_DEFAULT));
		imagen.setIcon(icono1);

		infoUI.add(imagen);
	}

	@Override
	public void initialize() {
		// TODO Auto-generated method stub

	}

}
