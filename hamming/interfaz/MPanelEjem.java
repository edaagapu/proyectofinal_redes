package hamming.interfaz;

import java.io.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import java.util.*;


public class MPanelEjem extends UIBPanelMenu{ 

	@Override
	public void addUIControls() {

		menuUI = new JPanel();
		menuUI.setBackground(Color.WHITE);
		menuUI.setLayout (null);
		
		menuUI.setBounds(2, 2, 510, 155);
		menuUI.setBorder(new LineBorder(new Color(0,95,194)));
        
		JLabel lblMenuQ = new JLabel("<html><font color=\"red\"><b> EJEMPLO HAMMING </font></html>");
        lblMenuQ.setBounds(210, 5, 180, 15);
        menuUI.add(lblMenuQ);
        
        JLabel lbl_Desc = new JLabel("");
        lbl_Desc.setBounds(10, 25, 490, 125);
         
        String txtDesc="<html>"
        		+"En este apartado puede conocer la implementacion del algoritmo de HAMMING "
        		+"donde se tiene en cuenta el procesos de codificaci�n mostrando paso a paso "
        		+"como se va ejecutando el mismo, en este caso para un ejemplo simple.  "
        		+"En la decodificaci�n se evaluara se usa el numero resultado de hamming mostrado, "
        		+"en la codificacion, "
        		+"entonces su proceso se hara de forma inversa para conocer la "
        		+"informacion de este o si en algun punto presento un error, que haya afectado"
        		+"un solo digito."
        		+"<p>" 
        		+"<i>Despues de leer este peque�o parrafo introducctorio, para "
        		+"encontrar mas informaci�n m�s detallada,puede mirar "
        		+" en el panel de abajo. </i>  "
        		+"</html>";
        lbl_Desc.setText(txtDesc);
        menuUI.add(lbl_Desc);
	}

	@Override
	public void initialize() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void paint(Graphics g) {
		// TODO Auto-generated method stub
		paint(g);
	}
	

}
