package huffman.logica;

public class Dato {
	public int frecuencia;
	public String caracter;
	public String codigo = "";
	
	public Dato(int frec, String car) {
		caracter = car;
		frecuencia = frec;
	}
}
