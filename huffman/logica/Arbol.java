package huffman.logica;

import java.util.ArrayList;

public class Arbol {
	private NodoArbol raiz;
	private Arbol izquierda, derecha;

	public Arbol(Dato informacion) {
		setRaiz(new NodoArbol(informacion.caracter,informacion.frecuencia));
		setIzquierda(null);
		setDerecha(null);
	}

	public int getFrecuencia() {
		return raiz.getFrecuencia();
	}

	public NodoArbol getRaiz() {
		return raiz;
	}

	public void setRaiz(NodoArbol raiz) {
		this.raiz = raiz;
	}

	public Arbol getIzquierda() {
		return izquierda;
	}

	public void setIzquierda(Arbol izquierda) {
		this.izquierda = izquierda;
	}

	public Arbol getDerecha() {
		return derecha;
	}

	public Arbol setDerecha(Arbol derecha) {
		this.derecha = derecha;
		return derecha;
	}

	public ArrayList<String> niveles() {
		ArrayList<String> resultado = new ArrayList<String>();
		Cola cola = new Cola();
		cola.shift(this);
		while (!cola.estaVacia()) {
			Arbol tempo = cola.unshift();
			resultado.add(tempo.getRaiz().getValor());
			if (tempo.getIzquierda() != null) {
				cola.shift(tempo.getIzquierda());
			}
			if (tempo.getDerecha() != null) {
				cola.shift(tempo.getDerecha());
			}
		}
		return resultado;
	}

	public int cantNiveles() {
		Arbol tempo = this;
		int recorrido = 1, recIzq = 0, recDer = 0;
		if (tempo.getIzquierda() != null || tempo.getDerecha() != null) {
			if (tempo.getIzquierda() != null) {
				recIzq = tempo.getIzquierda().cantNiveles();
			}
			if (tempo.getDerecha() != null) {
				recDer = tempo.getDerecha().cantNiveles();
			}
			if (recDer < recIzq) {
				return recorrido + recIzq;
			} else {
				return recorrido + recDer;
			}
		} else {
			return 0;
		}
	}
}
