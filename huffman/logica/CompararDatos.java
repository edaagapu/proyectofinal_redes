package huffman.logica;

import java.util.Comparator;

public class CompararDatos implements Comparator<Dato> {

	@Override
	public int compare(Dato datoU, Dato datoD) {
		if (datoU.frecuencia > datoD.frecuencia) {
			return -1;
		} else if (datoU.frecuencia < datoD.frecuencia) {
			return 0;
		} else if (datoU.frecuencia == datoD.frecuencia) {
			int charU = (int) datoU.caracter.charAt(0);
			int charD = (int) datoD.caracter.charAt(0);

			if (charU < charD) {
				return -1;
			} else if (charU > charD) {
				return 0;
			}
		}
		return 1;
	}

}
