package huffman.logica;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class TablaDatos {
	public ArrayList<Dato> datos = new ArrayList<Dato>();

	public TablaDatos(String palabra) {
		ArrayList<String> caracter = caracteres(palabra);
		ArrayList<Integer> frecuencia = new ArrayList<Integer>();

		for (int i = 0; i < caracter.size(); i++) {
			int frec = 0;
			for (int j = 0; j < palabra.length(); j++) {
				if (palabra.substring(j, j + 1).equals(caracter.get(i))) {
					frec++;
				}
			}
			frecuencia.add(frec);
		}
		for (int i = 0; i < frecuencia.size(); i++) {
			datos.add(new Dato(frecuencia.get(i), caracter.get(i)));
		}
		Collections.sort(datos, new CompararDatos());
	}

	public void agregar(Dato dato) {
		for (int i = 0; i < datos.size(); i++) {
			if (dato.frecuencia >= datos.get(i).frecuencia) {
				datos.add(i, dato);
				i = datos.size() + 1;
			}
		}
	}

	public ArrayList<String> caracteres(String palabra) {
		ArrayList<String> al = new ArrayList<>(); // add elements to al, including duplicates
		for (int i = 0; i < palabra.length(); i++) {
			al.add(palabra.substring(i, i + 1));
		}
		Set<String> hs = new HashSet<String>();
		hs.addAll(al);
		al.clear();
		al.addAll(hs);
		al.remove(" ");
		return al;
	}
}
