package huffman.logica;

public class NodoArbol {
	private String valor;
	private int frecuencia;
	private String codigo;

	public NodoArbol() {
		this.setValor(null);
		this.setCodigo(null);
		this.setFrecuencia(0);
	}

	public NodoArbol(String valor, int frecuencia) {
		this.setValor(valor);
		this.setFrecuencia(frecuencia);
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}
	public int getFrecuencia() {
		return frecuencia;
	}

	public void setFrecuencia(int frecuencia) {
		this.frecuencia = frecuencia;
	}
	
	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
}
