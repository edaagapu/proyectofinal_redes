package huffman.interfaz;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import java.awt.Color;

public class VMensaje extends JFrame {

	private JPanel contentPane;

	public VMensaje(String frase) {
		setTitle("Tu mensaje fue:");
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 12, 426, 211);
		contentPane.add(scrollPane);

		JTextArea txtrAcercaDe = new JTextArea();
		txtrAcercaDe.setLineWrap(true);
		txtrAcercaDe.setWrapStyleWord(true);
		txtrAcercaDe.setText("La frase introducida fue:" + "\n\n" + frase);
		txtrAcercaDe.setEditable(false);
		scrollPane.setViewportView(txtrAcercaDe);
	}
}
