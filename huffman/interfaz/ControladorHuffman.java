package huffman.interfaz;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import interfaz.VMenu;

public class ControladorHuffman implements ActionListener {
	private VHuffman ventana;

	public ControladorHuffman(VHuffman ventana) {
		this.ventana = ventana;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JButton boton = (JButton) e.getSource();
		for (int i = 0; i < ventana.btnVPrincipal.length; i++) {
			if (ventana.btnVPrincipal[i].equals(boton)) {
				accion(i);
			}
		}
	}

	private void accion(int i) {
		switch (i) {
		case 0:
			ventana.dispose();
			VEntradaHuffman entrada = new VEntradaHuffman();
			entrada.setVisible(true);
			break;
		case 1:
			VAyuda ayuda = new VAyuda();
			ayuda.setVisible(true);
			break;
		case 2:
			VMenu menu = new VMenu();
			menu.setVisible(true);
			ventana.dispose();
			break;

		default:
			break;
		}
	}

}
