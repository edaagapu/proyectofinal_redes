package huffman.interfaz;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import huffman.logica.TablaDatos;

public class PTabla extends JPanel {
	private JLabel[][] tabla = null;

	public PTabla(TablaDatos info, String frase) {
		setBackground(Color.WHITE);
		// setBounds(10, 10, width, height);
		setLayout(null);
		setBorder(new LineBorder(Color.BLACK));

		JLabel titulo = new JLabel("Tabla de datos");
		titulo.setBackground(getBackground());
		titulo.setBounds(10, 10, 280, 30);
		titulo.setFont(new Font("Arial", 1, 20));
		titulo.setHorizontalAlignment(SwingConstants.CENTER);
		add(titulo);

		JPanel PDatos = new JPanel();
		PDatos.setLayout(null);
		PDatos.setBackground(Color.WHITE);
		PDatos.setSize(300, 10 + info.datos.size() * 30);
		tabla = new JLabel[3][info.datos.size() + 1];
		for (int i = 0; i < tabla.length; i++) {
			for (int j = 0; j < tabla[i].length; j++) {
				tabla[i][j] = new JLabel();
				tabla[i][j].setBorder(getBorder());
				tabla[i][j].setBackground(getBackground());
				tabla[i][j].setBounds(10 + (i * 90), 10 + (j * 25), 91, 26);
				tabla[i][j].setHorizontalAlignment(SwingConstants.CENTER);
				PDatos.add(tabla[i][j]);
			}
		}
		tabla[0][0].setText("Simbolo");
		tabla[1][0].setText("Frecuencia");
		tabla[2][0].setText("Código");
		for (int j = 1; j < tabla[0].length; j++) {
			tabla[0][j].setText(info.datos.get(j - 1).caracter);
			tabla[1][j].setText(Integer.toString(info.datos.get(j - 1).frecuencia));
			tabla[2][j].setText(info.datos.get(j - 1).codigo);
		}
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(15, 50, 280, 325);
		PDatos.setPreferredSize(PDatos.getSize());
		scrollPane.setViewportView(PDatos);
		add(scrollPane);

		JButton btnFrase = new JButton("La frase codificada fue:");
		btnFrase.setBackground(getBackground());
		btnFrase.setBorder(new LineBorder(Color.BLACK));
		btnFrase.setBounds(10, 400, 280, 30);
		btnFrase.setFont(new Font("Arial", 1, 16));
		btnFrase.setHorizontalAlignment(SwingConstants.CENTER);
		btnFrase.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				VMensaje mensaje = new VMensaje(frase);
				mensaje.setVisible(true);
			}
		});
		add(btnFrase);

	}
}
