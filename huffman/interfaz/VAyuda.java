package huffman.interfaz;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import java.awt.Color;

public class VAyuda extends JFrame {

	private JPanel contentPane;

	public VAyuda() {
		setTitle("Ayuda Huffman");
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 12, 426, 211);
		contentPane.add(scrollPane);

		JTextArea txtrAcercaDe = new JTextArea();
		txtrAcercaDe.setLineWrap(true);
		txtrAcercaDe.setWrapStyleWord(true);
		txtrAcercaDe.setText(
				"ÁRBOLES DE HUFFMAN:\n\nEsta simulación muestra la codificación de Huffman con respecto a un mensaje, hallando la frecuencia en la que se repite cada caracter y organizandolo de mayor a menor, para formar un árbol binario, bajo el cual se genera un código que representa cada simbolo, cabe recalcar que en el presente proceso se encuentra organizado mediante el código ASCII (https://elcodigoascii.com.ar/), y se baso en el material trabajado en la materia de Redes de Comunicaciones II, en la Universidad Distrital junto a videos complementarios de YouTube (https://www.youtube.com/watch?v=zT2p-xD0NFw&t=96s).\n\nEl presente aplicativo no tiene en cuenta los espacios, para facilidad de la representación.");
		txtrAcercaDe.setEditable(false);
		scrollPane.setViewportView(txtrAcercaDe);
	}
}
