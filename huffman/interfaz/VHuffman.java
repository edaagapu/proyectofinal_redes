package huffman.interfaz;

import java.awt.Color;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;

import huffman.logica.Arbol;
import huffman.logica.Dato;
import huffman.logica.TablaDatos;

public class VHuffman extends JFrame {

	private PArbol pnlArbol;
	private JPanel contentPane;
	private PTabla pnlTabla;
	private int parametro = 1;
	private String textoBotones[] = { "Reiniciar", "Ayuda", "Volver" };
	public JButton btnVPrincipal[] = new JButton[textoBotones.length];
	private ArrayList<NodoGrafico[]> nodos = null;

	public VHuffman(String texto) {
		setTitle("Árboles de Huffman");
		ControladorHuffman controlador = new ControladorHuffman(this);
		Arbol arbol = construirArbol(texto);
		parametro = arbol.cantNiveles();
		pnlArbol = new PArbol((int) (Math.pow(2, parametro) * 70), ((parametro + 1) * 50));
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);

		setContentPane(contentPane);
		contentPane.setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 10, 750, 450);
		pnlArbol.setPreferredSize(pnlArbol.getSize());
		contentPane.add(scrollPane);

		pnlTabla.setBounds(780, 10, 310, 450);
		contentPane.add(pnlTabla);

		nodos = new ArrayList<NodoGrafico[]>();
		for (int i = 0; i <= parametro + 1; i++) {
			nodos.add(new NodoGrafico[(int) (Math.pow(2, i))]);
			for (int j = 0; j < nodos.get(i).length; j++) {
				nodos.get(i)[j] = new NodoGrafico("",
						(int) (pnlArbol.getWidth() / Math.pow(2, i + 1)) * ((2 * j) + 1) - 25, 10 + 50 * i);
				pnlArbol.add(nodos.get(i)[j]);
			}
		}
		for (int i = 0; i < nodos.get(nodos.size() - 1).length; i++) {
			pnlArbol.remove(nodos.get(nodos.size() - 1)[i]);
		}
		nodos.remove(nodos.size() - 1);
		adaptarArbol(arbol, 0, 0);
		for (int i = 0; i < nodos.size(); i++) {
			for (int j = 0; j < nodos.get(i).length; j++) {
				if (nodos.get(i)[j].getText().equals("")) {
					pnlArbol.remove(nodos.get(i)[j]);
					nodos.get(i)[j] = null;
				}
			}
		}

		pnlArbol.setNodos(nodos);
		pnlArbol.repaint();
		scrollPane.setViewportView(pnlArbol);
		setBounds(50, 50, 1100, 575);
		for (int i = 0; i < btnVPrincipal.length; i++) {
			btnVPrincipal[i] = new JButton(textoBotones[i]);
			btnVPrincipal[i].setBorder(pnlArbol.getBorder());
			btnVPrincipal[i].setBackground(Color.WHITE);
			btnVPrincipal[i].setBounds(200 + (300 * i), 500, 130, 30);
			btnVPrincipal[i].addActionListener(controlador);
			contentPane.add(btnVPrincipal[i]);
		}
	}

	private Arbol construirArbol(String texto) {
		ArrayList<Arbol> arbol = new ArrayList<Arbol>();
		TablaDatos info = new TablaDatos(texto);
		for (int i = 0; i < info.datos.size(); i++) {
			arbol.add(new Arbol(info.datos.get(i)));
		}
		while (arbol.size() > 1) {
			Arbol izq = arbol.remove(arbol.size() - 1);
			Arbol der = arbol.remove(arbol.size() - 1);
			Dato reconstruido = new Dato(izq.getRaiz().getFrecuencia() + der.getRaiz().getFrecuencia(),
					izq.getRaiz().getValor() + der.getRaiz().getValor());
			Arbol nuevo = new Arbol(reconstruido);
			nuevo.setIzquierda(izq);
			nuevo.setDerecha(der);
			if (arbol.size() == 0) {
				arbol.add(nuevo);
			} else {
				for (int i = 0; i < arbol.size(); i++) {
					if (nuevo.getFrecuencia() >= arbol.get(i).getFrecuencia()) {
						arbol.add(i, nuevo);
						i = arbol.size() + 1;
					}
				}
			}
		}
		// Generando los códigos
		for (int i = 0; i < info.datos.size(); i++) {
			String codigo = "";
			Arbol arbolito = arbol.get(0);
			while (arbolito.getIzquierda() != null && arbolito.getDerecha() != null
					&& !arbolito.getRaiz().getValor().equals(info.datos.get(i).caracter)) {
				if (arbolito.getIzquierda().getRaiz().getValor().indexOf(info.datos.get(i).caracter) != -1) {
					codigo = codigo + "0";
					arbolito = arbolito.getIzquierda();
				} else {
					codigo = codigo + "1";
					arbolito = arbolito.getDerecha();
				}
			}
			info.datos.get(i).codigo = codigo;
		}
		pnlTabla = new PTabla(info, texto);
		return arbol.get(0);
	}

	private void adaptarArbol(Arbol arbol, int i, int j) {
		if (arbol != null) {
			nodos.get(i)[j].setText(Integer.toString(arbol.getRaiz().getFrecuencia()));
			if (arbol.getRaiz().getValor().length() == 1) {
				JLabel lblTexto = new JLabel(arbol.getRaiz().getValor());
				lblTexto.setBounds(nodos.get(i)[j].getX(), nodos.get(i)[j].getY() + nodos.get(i)[j].getHeight(), 50,
						30);
				lblTexto.setForeground(Color.RED);
				lblTexto.setHorizontalAlignment(SwingConstants.CENTER);
				pnlArbol.add(lblTexto);
			}
			int tempoU = i, tempoD = j;
			Arbol centi = arbol;
			while (centi.getIzquierda() != null) {
				centi = centi.getIzquierda();
				i++;
				j = 2 * j;
				adaptarArbol(centi, i, j);
			}
			centi = arbol;
			i = tempoU;
			j = tempoD;
			while (centi.getDerecha() != null) {
				centi = centi.getDerecha();
				i++;
				j = 2 * j + 1;
				adaptarArbol(centi, i, j);
			}
		}
	}
}
