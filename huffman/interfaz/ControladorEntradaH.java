package huffman.interfaz;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class ControladorEntradaH implements KeyListener {

	@Override
	public void keyPressed(KeyEvent key) {
	}

	@Override
	public void keyReleased(KeyEvent key) {
	}

	@Override
	public void keyTyped(KeyEvent key) {
		char caracter = key.getKeyChar();
		int val = (int) caracter;
		if (val == 32) {
			key.consume();
		}
	}

}
