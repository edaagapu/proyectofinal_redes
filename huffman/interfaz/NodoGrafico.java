package huffman.interfaz;

import java.awt.Color;

import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

public class NodoGrafico extends JLabel {
	public NodoGrafico(String texto, int x, int y) {
		setText(texto);
		setHorizontalAlignment(SwingConstants.CENTER);
		setBounds(x, y, 50, 30);
		setBorder(new LineBorder(Color.BLACK));
	}
}
