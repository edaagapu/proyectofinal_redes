package manchester.interfaz;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Color;
import javax.swing.border.LineBorder;

import manchester.logica.Codigo;

import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class VEntradaManchester extends JFrame {

	private JPanel contentPane;
	private JTextField textField;

	public VEntradaManchester() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 300, 160);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new LineBorder(new Color(0, 0, 0)));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel.setBackground(Color.WHITE);
		panel.setBounds(10, 10, 280, 110);
		contentPane.add(panel);
		panel.setLayout(null);

		JLabel lblTitulo = new JLabel("Inserte el código");
		lblTitulo.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 12));
		lblTitulo.setHorizontalAlignment(SwingConstants.CENTER);
		lblTitulo.setBounds(10, 10, 270, 25);
		panel.add(lblTitulo);

		textField = new JTextField();
		textField.setBorder(new LineBorder(new Color(0, 0, 0)));
		textField.setBackground(Color.WHITE);
		textField.setBounds(80, 40, 120, 20);
		textField.addKeyListener(new ControladorEntradaM());
		panel.add(textField);

		JButton btnNewButton = new JButton("Aceptar");
		btnNewButton.setBorder(new LineBorder(new Color(0, 0, 0)));
		btnNewButton.setBackground(Color.WHITE);
		btnNewButton.setBounds(100, 75, 80, 25);
		btnNewButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (textField.getText().length() > 0) {
					VManchester vManchester = new VManchester(new Codigo(textField.getText()));
					vManchester.setVisible(true);
					dispose();
				} else {
					JOptionPane.showMessageDialog(null,
							"Recuerde ingresar el código.\nSólo son válidos los digitos 1 y 0");
				}
			}
		});
		panel.add(btnNewButton);
	}
}
