package manchester.interfaz;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import java.awt.Color;

public class VAyuda extends JFrame {

	private JPanel contentPane;

	public VAyuda() {
		setTitle("Ayuda Manchester");
		setBounds(100, 100, 548, 298);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 12, 234, 210);
		contentPane.add(scrollPane);

		JTextArea txtrManchester = new JTextArea();
		txtrManchester.setLineWrap(true);
		txtrManchester.setWrapStyleWord(true);
		txtrManchester.setText(
				"CODIFICACIÓN MANCHESTER\n\nLa codificación Manchester o hackneo provee una forma simple de codificar secuencias de bits, incluso cuando hay largas secuencias de periodos sin transiciones de nivel que puedan significar la pérdida de sincronización, o incluso errores en las secuencias de bits. Esta codificación también nos asegura que la componente continua de las señales es cero si se emplean valores positivos y negativos para representar los niveles de la señal, haciendo más fácil la regeneración de la señal, y evitando las pérdidas de energía de las señales.");
		txtrManchester.setEditable(false);
		scrollPane.setViewportView(txtrManchester);

		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(304, 12, 234, 210);
		contentPane.add(scrollPane_1);

		JTextArea txtrManchesterD = new JTextArea();
		txtrManchesterD.setWrapStyleWord(true);
		txtrManchesterD.setLineWrap(true);
		txtrManchesterD.setText(
				"CÓDIGO DE MANCHESTER DIFERENCIAL\n\nLa codificación de Manchester diferencial permite hacer la sincronización en la mitad  del bit, y se comporta de la siguiente manera: \n\n- Si el dato transmitido es 1: No habrá transición en el primer bit.\n- Si el dato transmitido es 0: Se realizará una transición en el primer bit.\n\nCabe recalcar que a mitad del bit, siempre existirá una transición independientemente del dato transmitido, por lo que esa transición implica la sincronización.\n");
		txtrManchesterD.setEditable(false);
		scrollPane_1.setViewportView(txtrManchesterD);
	}
}
