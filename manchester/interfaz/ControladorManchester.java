package manchester.interfaz;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import interfaz.VMenu;

public class ControladorManchester implements ActionListener {
	private VManchester ventana;

	public ControladorManchester(VManchester ventana) {
		this.ventana = ventana;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JButton boton = (JButton) e.getSource();
		for (int i = 0; i < ventana.btnMenu.length; i++) {
			if (ventana.btnMenu[i].equals(boton)) {
				accion(i);
			}
		}
	}

	private void accion(int i) {
		switch (i) {
		case 0:
			ventana.dispose();
			VEntradaManchester entrada = new VEntradaManchester();
			entrada.setVisible(true);
			break;
		case 1:
			VAyuda ayuda = new VAyuda();
			ayuda.setVisible(true);
			break;
		case 2:
			VMenu menu = new VMenu();
			menu.setVisible(true);
			ventana.dispose();
			break;

		default:
			break;
		}
	}

}
