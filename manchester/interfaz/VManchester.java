package manchester.interfaz;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import manchester.logica.Codigo;

public class VManchester extends JFrame {

	private JPanel contentPane;
	private JScrollPane[] pnScroll = new JScrollPane[3];
	private PDibujo[] dibujo = new PDibujo[3];
	public JButton[] btnMenu = new JButton[3];
	private JLabel[] lblTitulo = new JLabel[3];
	private String[] texto = { "Datos", "Codigo Manchester", "Codigo Manchester Diferencial" };
	private String[] txtBotones = { "Reiniciar", "Ayuda", "Volver" };

	public VManchester(Codigo codigo) {
		setTitle("Codificación de Manchester");
		setBounds(10, 10, 1242, 310);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.setBackground(Color.WHITE);

		ControladorManchester controlador = new ControladorManchester(this);

		dibujo[0] = new PDibujo(codigo.getCodigo(), 50, false, codigo.getCodigo());
		dibujo[1] = new PDibujo(codigo.getCodigoManchester(), 25, true, codigo.getCodigo());
		dibujo[2] = new PDibujo(codigo.getCodigoManchesterD(), 25, true, codigo.getCodigo());

		for (int i = 0; i < pnScroll.length; i++) {
			lblTitulo[i] = new JLabel(texto[i]);
			lblTitulo[i].setFont(new Font("Arial", 1, 14));
			lblTitulo[i].setBounds(10 + (410 * i), 10, 400, 30);
			pnScroll[i] = new JScrollPane();
			pnScroll[i].setBounds(10 + (410 * i), 50, 400, 160);
			pnScroll[i].setViewportView(dibujo[i]);

			contentPane.add(pnScroll[i]);
			contentPane.add(lblTitulo[i]);
		}
		for (int i = 0; i < btnMenu.length; i++) {
			btnMenu[i] = new JButton(txtBotones[i]);
			btnMenu[i].setBounds(150 + (414 * i), 240, 114, 30);
			btnMenu[i].setBorder(new LineBorder(Color.BLACK));
			btnMenu[i].setBackground(Color.WHITE);
			btnMenu[i].addActionListener(controlador);
			contentPane.add(btnMenu[i]);
		}
	}
}
