package manchester.interfaz;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class ControladorEntradaM implements KeyListener {

	@Override
	public void keyPressed(KeyEvent key) {
	}

	@Override
	public void keyReleased(KeyEvent key) {
	}

	@Override
	public void keyTyped(KeyEvent key) {
		char caracter = key.getKeyChar();
		int val = (int) caracter;
		if (val < 48 || val > 49) {
			key.consume();
		}
	}

}
