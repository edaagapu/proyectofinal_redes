package manchester.interfaz;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class PDibujo extends JPanel {
	public int[] codigo;
	public int[] deco;
	public int interseccion;
	public boolean manchester;

	public PDibujo(int[] codigo, int interseccion, boolean manchester, int[] deco) {
		setBackground(Color.WHITE);
		setLayout(null);
		setBorder(null);
		this.codigo = codigo;
		this.deco = deco;
		this.interseccion = interseccion;
		if (manchester) {
			setPreferredSize(new Dimension((codigo.length + 8) * interseccion, 130));
		} else {
			setPreferredSize(new Dimension((codigo.length + 4) * interseccion, 130));
		}
		this.manchester = manchester;
	}

	public void paint(Graphics g) {
		super.paint(g);
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, getPreferredSize().width, getPreferredSize().height);
		g.setColor(Color.BLACK);

		g.drawLine(40, 120, 40, 30);
		g.drawLine(30, 60, 40, 60);
		g.drawString("1", 15, 60);
		if (manchester) {
			g.drawLine(40, 120, 40 + ((codigo.length + 4) * interseccion), 120);
			int conteo = 0;
			for (int i = 2; i < codigo.length + 2; i = i + 2) {
				dibujarLineaIntermitente(g, 40 + (interseccion * i), 30, 120);
				g.setColor(Color.GRAY);
				g.drawString(Integer.toString(deco[conteo]), 40 + interseccion + (interseccion * i), 30);
				conteo++;
			}
		} else {
			g.drawLine(40, 120, 40 + ((codigo.length + 2) * interseccion), 120);
			for (int i = 1; i < codigo.length + 1; i++) {
				dibujarLineaIntermitente(g, 40 + (interseccion * i), 30, 120);
				g.setColor(Color.GRAY);
				g.drawString(Integer.toString(codigo[i - 1]), 40 + (interseccion / 2) + (interseccion * i), 30);
			}
		}
		dibujarCodigo(g);
	}

	public void dibujarLineaIntermitente(Graphics g, int x, int y, int yF) {
		int bandera = y;
		g.setColor(Color.BLUE);
		while (bandera < yF) {
			g.drawLine(x, bandera, x, bandera + 5);
			bandera = bandera + 10;
		}
	}

	public void dibujarCodigo(Graphics g) {
		g.setColor(Color.RED);
		int base = 0;
		int bandera = 0;
		int[] codigo;
		if (manchester) {
			codigo = new int[this.codigo.length + 2];
			codigo[1] = 0;
			bandera = 2;
		} else {
			codigo = new int[this.codigo.length + 1];
			bandera = 1;
		}
		codigo[0] = 0;
		for (int i = bandera; i < codigo.length; i++) {
			codigo[i] = this.codigo[i - bandera];
		}
		for (int i = bandera; i < codigo.length; i++) {
			if (codigo[i] == 0) {
				base = 120;
			} else {
				base = 60;
			}

			if (codigo[i] == codigo[i - 1]) {
				g.drawLine(40 + (interseccion * i), base, 40 + (interseccion * (i + 1)), base);
			} else {
				g.drawLine(40 + (interseccion * i), 120, 40 + (interseccion * i), 60);
				g.drawLine(40 + (interseccion * i), base, 40 + (interseccion * (i + 1)), base);
				if ((i < codigo.length - 1 && codigo[i] != codigo[i + 1])) {
					g.drawLine(40 + (interseccion * (i + 1)), 120, 40 + (interseccion * (i + 1)), 60);
				}
			}
			if (i == (codigo.length - 1) && codigo[i] == 1) {
				g.drawLine(40 + (interseccion * (i + 1)), 120, 40 + (interseccion * (i + 1)), 60);
			}
		}

	}
}
