package manchester.logica;

import java.util.ArrayList;

public class Codigo {
	int[] codigo;

	public Codigo(String codigo) {
		this.codigo = new int[codigo.length()];
		for (int i = 0; i < codigo.length(); i++) {
			this.codigo[i] = Integer.parseInt(codigo.substring(i, i + 1));
		}
	}

	public int[] getCodigo() {
		return codigo;
	}

	public int[] getCodigoManchester() {
		ArrayList<Integer> manchester = new ArrayList<Integer>();
		for (int i = 0; i < codigo.length; i++) {
			if (codigo[i] == 0) {
				manchester.add(0);
				manchester.add(1);
			} else {
				manchester.add(1);
				manchester.add(0);
			}
		}
		int[] resultado = new int[manchester.size()];
		for (int i = 0; i < manchester.size(); i++) {
			resultado[i] = manchester.get(i);
		}
		return resultado;
	}

	public int[] getCodigoManchesterD() {
		ArrayList<Integer> manchesterD = new ArrayList<Integer>();
		manchesterD.add(0);
		for (int i = 0; i < codigo.length; i++) {
			if (codigo[i] == 1) {
				if (manchesterD.get(manchesterD.size() - 1) == 0) {
					manchesterD.add(0);
					manchesterD.add(1);
				} else {
					manchesterD.add(1);
					manchesterD.add(0);
				}
			} else {
				if (manchesterD.get(manchesterD.size() - 1) == 0) {
					manchesterD.add(1);
					manchesterD.add(0);
				} else {
					manchesterD.add(0);
					manchesterD.add(1);
				}
			}
		}
		manchesterD.remove(0);
		int[] resultado = new int[manchesterD.size()];
		for (int i = 0; i < manchesterD.size(); i++) {
			resultado[i] = manchesterD.get(i);
		}
		return resultado;
	}
}
